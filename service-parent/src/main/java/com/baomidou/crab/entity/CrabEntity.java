package com.baomidou.crab.entity;

import java.time.LocalDateTime;

import com.baomidou.crab.core.bean.BaseEntity;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业务框架基础实体
 * </p>
 *
 * @author jobob
 * @since 2018-10-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CrabEntity extends BaseEntity {

    @ApiModelProperty(value = "删除 0、否 1、是")
    @TableLogic
    private Boolean deleted;

    @ApiModelProperty(value = "操作人")
    @TableField(fill = FieldFill.INSERT)
    private String operator;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

}
