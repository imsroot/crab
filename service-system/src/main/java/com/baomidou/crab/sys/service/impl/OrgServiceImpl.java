package com.baomidou.crab.sys.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.crab.sys.entity.Org;
import com.baomidou.crab.sys.mapper.OrgMapper;
import com.baomidou.crab.sys.service.IOrgService;
import com.baomidou.mybatisplus.core.conditions.Condition;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * <p>
 * 系统机构表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2018-11-07
 */
@Service
public class OrgServiceImpl extends ServiceImpl<OrgMapper, Org> implements IOrgService {


    @Override
    public List<Org> listByCompanyId(Long companyId) {
        return super.list(Wrappers.<Org>query().select("id", "pid", "name")
                .eq("company_id", companyId).orderByDesc("sort"));
    }
}
