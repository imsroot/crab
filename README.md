# crab

#### 项目介绍
crab web

- 演示地址：http://crab.baomidou.com

#### 启动说明
- Idea 安装 lombok 设置动态编译【不会百度】
- Gradle 4+ 安装环境变量配置【不会百度】
- 群附件下载 crab-core-0.1.jar 导入本地仓库，坐标 com.baomidou:crab-core:0.1
- 导入 sql 修改 service-system 对应 yml 配置
- 导入 crab 运行 SystemApplication.java 点击控制台访问目录
- 关注 crab 项目 Star 截图与复制启动控制台激活码发送邮件 jobob@qq.com 索取授权 License
